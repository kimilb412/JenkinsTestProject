package com.test.org.main;

import java.security.spec.ECField;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	private Map<Integer, Integer> tableMap = new Hashtable<Integer, Integer>();
	private Map<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
	private Map<Integer, Integer> conMap = new ConcurrentHashMap<Integer, Integer>();
	
	public static void main(String[] args) {
		new Main().doTask();
	}

	private void doTask() {
		
		ExecutorService e = Executors.newFixedThreadPool(3);
		e.execute(new Task1("task1", 0));
		e.execute(new Task1("task2", 1));
		e.execute(new Task2("task3", 0));
		
		e.shutdown();
		System.out.println("All done");
	}
	
	private class Task1 implements Runnable {
		
		private String taskName;
		private int index;
		
		public Task1(String name, int index) {
			this.taskName = name;
			this.index = index;
		}
		
		public void run() {
			for(int i = 0 + this.index*430; i < 430+this.index*430; i++) {
				tableMap.put(i, i);
				hashMap.put(i, i);
				conMap.put(i, i);
				
				System.out.println(this.taskName + " read!!!");
			}
			
			System.out.println(this.taskName + " is done");
		}		
	}
	
	private class Task2 implements Runnable {

		private StringBuffer sb1, sb2, sb3 ;
		private String taskName;
		private int index;
		
		public Task2(String name, int index) {
			sb1 = new StringBuffer();
			sb2 = new StringBuffer();
			sb3 = new StringBuffer();
			taskName = name;
			this.index = index;
		}
		
		public void run() {
			for(int i = 0+this.index*430; i < 430+this.index*430; i++) {
				sb1.append(tableMap.get(i)).append(",");
				sb2.append(hashMap.get(i)).append(",");
				sb3.append(conMap.get(i)).append(",");
				
				System.out.println(this.taskName + " write!!");
			}
			
			System.out.println(sb1.toString());
			System.out.println(sb2.toString());
			System.out.println(sb3.toString());
			System.out.println("-----------------------------------------------------------------");
			
			System.out.println(this.taskName + " is done ");
		}
	}
}
